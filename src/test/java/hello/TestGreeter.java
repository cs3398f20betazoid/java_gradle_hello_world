package hello;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


// Change the comment again to kick off a build

// JOJO ADDED THIS COMMENT THRU THE JOJO BRANCH!!!

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }



   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
	  
   }
   
   
   @Test
   @DisplayName("Test for Name='Johana Di Girolamo'")
   public void testGreeterJohana() 
   {
	  g.setName("Johana Di Girolamo");
      assertEquals(g.getName(),"Johana Di Girolamo");
      assertEquals(g.sayHello(),"Hello Johana Di Girolamo!");
   }
   
   @Test
   @DisplayName("Test for Name='Jojo!'")
   public void testGreeterAssertFalse() 
   {
	  g.setName("Jojo");
      assertFalse(g.getName()!="Jojo","Johana Di Girolamo");
   }

   @Test
   @DisplayName("Test for Name='Blane Rhodes'")
   public void testGreeterBlane(){
      g.setName("Blane Rhodes");
      assertEquals(g.getName(), "Blane Rhodes");
      assertEquals(g.sayHello(), "Hello Blane Rhodes!");
   }

   @Test
   @DisplayName("This bad boy is false")
   public void testAssertFalse(){
      g.setName("Blane");
      String name = g.getName();
      assertFalse(name.equals("Not Blane"));
   }


  @Test
  @DisplayName("Test for Name='MaryKathryn'")
  public void testGreeterMary()
  {
     g.setName("MaryKathryn");
     assertEquals(g.getName(),"MaryKathryn");
     assertEquals(g.sayHello(),"Hello MaryKathryn!");
  }

  @Test
  @DisplayName("Test for Name='SeeYa'")
  public void testGoodbye()
  {
     g.setName("SeeYa");
     assertEquals(g.getName(),"SeeYa");
     assertEquals(g.sayHello(),"Hello SeeYa!");
     assertFalse(g.getName() == "Yikes");
  }


}