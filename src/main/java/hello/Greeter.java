package hello;

// adding comment
// adding a live comment only on my branch

public class Greeter {

  private String name = "";


  public String getName() 

  {
    return name;
  }


  public void setName(String name) 

  {
      this.name = name;
  }


  public String sayHello() 

  {
  	if (name == "") 

    {
       return "Hello!";
    }
    else 
    {
       return "Hello " + name + "!";
    }

  }

}
